#!/usr/bin/env bash

case $1 in

  --enable)
      enable=1
      msg="ALPS DualPoint Stick Enable!\n";;
  --disable)
      enable=0
      msg="ALPS DualPoint Stick Disable!\n";;
  *)
      printf "\nOpção inválida! utilize [ --enable | --disable ]";;

esac	

{
  dualpoint=$(xinput list |grep "ALPS DualPoint Stick" |awk -F' ' '{ print $07 }' |cut -d"=" -f2)
  /usr/bin/xinput set-prop ${dualpoint} "Device Enabled" ${enable}
} &> /dev/null

printf "\n${msg}\n"
