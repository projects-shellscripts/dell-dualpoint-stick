### ALPS DualPoint Stick ###

```
Enable and Disable Dell DualPoint Stick
```

```Test performed on Ubuntu 16.04.3 LTS```

 - **How to use**

```
--enable | --disable

Ex: ~]# bash dell_dualpoint_stick.sh --disable
```
